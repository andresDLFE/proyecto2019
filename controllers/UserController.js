const requestJson = require('request-json');

const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/proyecto2019/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

const accountController = require('./AccountController');

//
// GET USER
//
function getUsers(req, res) {
  console.log("GET /proyecto2019/users");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para GET");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body) {
        var response = !err ? body : { // decide ternario
          "msg" : "error obteniendo usuarios"
        }
        res.send(response);
    }
  )
}
//
// GET USER BY ID
//
function getUsersById(req, res) {
  console.log("GET /proyecto2019/users/:id");
  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log("La query es :" + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para GET BY ID");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
        if (err) {
            var response = {
              "msg" : "error obteniendo usuario by ID"
            }
            res.status(500);
        } else {
          if (body.length > 0) {
            var response = body[0];
          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
        res.send(response);
    }
  )
}
//
// POST create user
//
function createUser(req, res) {
  console.log("POST /create/users con GET MAX");
  var query = 's={"id": -1}&l=1';
  console.log("La query es :" + query);
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para GET MAX");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
        if (err) {
            console.log("msg :" + "error acceso a datos")
                 }
         else {
          if (body.length > 0) {
            console.log("id max: " + body[0].id);
            console.log("      ");
            nuevoId =  body[0].id + 1;
            console.log("Id Max :" + nuevoId);
                               }
            else {nuevoId = 1;}

            console.log("Nuevo id del nuevo cliente :" + nuevoId);
            console.log(req.body.first_name);
            console.log(req.body.last_name);
            console.log(req.body.email);
            console.log(req.body.password);

            var newUser = {
              "id": nuevoId,
              "first_name": req.body.first_name,
              "last_name": req.body.last_name,
              "email": req.body.email,
              "password": crypt.hash(req.body.password),
              "logged": true
            }

            var httpClient = requestJson.createClient(baseMLabURL);
            console.log("Cliente creado para POST BY ID V2");

            httpClient.post("user?" + mLabAPIKey, newUser,
              function(err, resMLab, body) {
                    console.log("Usuario creado con exito en MLab");
                    res.send(newUser);
              }
            )
            }
      }
    )
}

module.exports.getUsers=getUsers;
module.exports.getUsersById=getUsersById;
module.exports.createUser=createUser;
