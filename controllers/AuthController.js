const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyecto2019/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');

function loginUser(req, res) {
  console.log("POST /proyecto2019/login");

  console.log(req.body.email);
  console.log(req.body.password);

  var query = 'q={"email": "' + req.body.email + '"}';
  console.log("La consulta es " + query);

  // crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, bodyGet) {
      if (bodyGet.length > 0) {
        var response = bodyGet[0];
        console.log("req.body.password: " + req.body.password);
        console.log("bodyGet.password: " + bodyGet[0].password);
        var encontrado = crypt.checkPassword(req.body.password, bodyGet[0].password);
        console.log("Encontrado: " + encontrado);
        if (encontrado) {
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey,
            JSON.parse(putBody))
          var response = {"msg" : "login correcto" , "idUsuario" : bodyGet[0].id}
        } else {
          var response = {"msg" : "Password incorrecta"}
          res.status(400);
        }
      } else {
        var response = {"msg" : "Usuario no encontrado"}
        res.status(500);
      }
      res.send(response);
    }
  )

}


function logoutUser(req, res) {
  console.log("POST /proyecto2019/logout/:id");
  console.log("llega Id?" +req.body.id);

  var query = 'q={"id": ' + req.body.id + '}';
  console.log("La consulta es " + query);

  // crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, bodyGet) {
      if (bodyGet.length > 0) {
        console.log("Usuario encontrado: " + req.body.id);

        if (bodyGet[0].logged == true) {
          console.log("Usuario conectado");
          var putBody = '{"$unset":{"logged":""}}'
          httpClient.put("user?" + query + "&" + mLabAPIKey,
             JSON.parse(putBody))
          var response = {"mensaje" : "logout correcto" , "idUsuario" : bodyGet[0].id}
        } else {
          var response = {"msg" : "Usuario no conectado"}
        }
      } else {
        var response = {"msg" : "Usuario no encontrado"}
      }
      res.send(response);
    }
  )
}


function logoutUser2(req, res) {
  console.log("POST /proyecto2019/logout/:id");
  console.log("llega Id?" +req.body.IBAN);
  var IBAN = req.body.IBAN;
  var query = 'q={"IBAN":' + '"' + IBAN + '"' + '}';
  console.log("La query del movimiento :" + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para GET MOVEMENT BY IBAN");

  httpClient.get("movement?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
        if (err) {
            var response = {
              "msg" : "error obteniendo movimientos by IBAN"
            }
            res.status(500);
        } else {
          if (body.length > 0) {
            var response = body;
            console.log("Importe :" + body[0].imPoper); 
          } else {
            var response = {
              "msg" : "Movimiento no encontrado"
            }
            res.status(404);
          }
        }
        res.send(response);
    }
  )
}

module.exports.loginUser = loginUser;
module.exports.logoutUser = logoutUser;
module.exports.logoutUser2 = logoutUser2;
