const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyecto2019/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');
const fecha = require('../fecha');
const accountController = require('./AccountController');


function getMovementsByIBAN(req, res) {
  console.log("POST /proyecto2019/logout/:id");
  console.log("llega Id?" +req.body.IBAN);
  var IBAN = req.body.IBAN;
  var query = 'q={"IBAN":' + '"' + IBAN + '"' + '}';
  console.log("La query del movimiento :" + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para GET MOVEMENT BY IBAN");

  httpClient.get("movement?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
        if (err) {
            var response = {
              "msg" : "error obteniendo movimientos by IBAN"
            }
            res.status(500);
        } else {
          if (body.length > 0) {
            var response = body;
            console.log("Importe :" + body[0].imPoper);
          } else {
            var response = {
              "msg" : "Movimiento no encontrado"
            }
            res.status(404);
          }
        }
        res.send(response);
    }
  )
}

function altaMovement(req, res) {
  console.log("1. POST /create/movement ");
  console.log(req.body.IBAN);
  console.log(req.body.imPoper);
  console.log(req.body.concepto);

  var today = fecha.obtenerFecha();

  if (req.body.concepto == "Ingreso"){
     var importe = req.body.imPoper * (1)
   } else {
     var importe = req.body.imPoper * (-1);
   }

  var newMovement = {
    "IBAN": req.body.IBAN,
    "imPoper": importe,
    "fecPoper": today,
    "concepto": req.body.concepto
  }

  console.log("voy a insertMovement" + importe);

  insertMovement(newMovement, res);

  console.log("3. Vuelvo de insertMovement");

  console.log("4. Voy a apdateAccount");
  console.log("5. IBAN        :" + req.body.IBAN);
  console.log("5.2 Importe    :" + importe);

  accountController.updateAccount(req.body.IBAN, importe);

  console.log("7. Vuelvo de updateAccount / Modificar saldo");

  function insertMovement(newMovement, res) {
      console.log("2.1 *** En la función insertMovement ***");
      console.log(newMovement.IBAN);
      console.log(newMovement.imPoper);
      console.log(newMovement.fecPoper);
      console.log(newMovement.concepto);

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("2.2    Cliente creado para POST insertMovement");

      httpClient.post("movement?" + mLabAPIKey, newMovement,
      function(err, resMLab, body) {
               console.log("2.3    Movimiento creado con exito en MLab");
               res.status(201).send({"msg":"Movimiento guardado"});
      }
    )
  }



}

module.exports.altaMovement = altaMovement;
module.exports.getMovementsByIBAN = getMovementsByIBAN;
