const requestJson = require('request-json');
const fecha = require('../fecha');

const baseMLabURL = "https://api.mlab.com/api/1/databases/proyecto2019/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
//
// GET ACCOUNT BY ID
//
function getAccountById(req, res) {
  console.log("GET /proyecto2019/account/:idUsuario");
//  var idUsuario = req.body.idUsuario;
  var idUsuario = req.params.idUsuario;
  var query = 'q={"idUsuario":' + idUsuario + '}';
  console.log("La query es :" + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para GET ACCOUNT BY ID");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
        if (err) {
            var response = {
              "msg" : "error obteniendo cuenta by idUsuario V2"
            }
            res.status(500);
        } else {
          if (body.length > 0) {
            var response = body;
          } else {
            var response = {
              "msg" : "Usuario sin cuentas"
            }
            res.status(404);
          }
        }
        res.send(response);
    }
  )
}

function altaContrato(req, res){
  var nuevoId = req.body.idUsuario;
  console.log("El idUsuario :" + nuevoId);
  var today = fecha.obtenerFecha();

  console.log("---> Fecha en codigo : " + today);

  var num1 = Math.random() * 100 + 1;
  var num2 = Math.random() * 10000;
  var num3 = Math.random() * 10000;
  var num4 = Math.random() * 10000;
  var num5 = Math.random() * 10000;
  var num6 = Math.random() * 10000;

  var iban = "ES" + num1.toFixed(0) +
             "  " + num2.toFixed(0) +
             "  " + num3.toFixed(0) +
             "  " + num4.toFixed(0) +
             "  " + num5.toFixed(0) +
             "  " + num6.toFixed(0);

  console.log("IBAN :" + iban);

  var newAccount = {
    "idUsuario": nuevoId,
    "IBAN": iban,
    "balance": 0.00,
    "fecApertura": today
  }

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para POST account");

  httpClient.post("account?" + mLabAPIKey, newAccount,
    function(err, resMLab, body) {
          console.log("Account creada con exito en MLab" + iban + nuevoId);
          res.status(201).send({"msg":"Cuenta creada"});
    }
  )
}

function getAccountByIBAN(req, res){

  console.log("GET /proyecto2019/account");
  console.log("IBAN :" + req.body.IBAN);

  var query = 'q={"IBAN":' + '"' + req.body.IBAN + '"' + '}';
  console.log("getAccountByIBAN La query es :" + query);
  //
  //Crea cliente http
  //
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado para GET ACCOUNT BY IBAN");
  //
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
            var response = {
              "msg" : "error obteniendo cuenta by IBAN"
            }
            res.status(500);
        } else {
          console.log("body   : " + body[0]);
          console.log("length : " + body.length);

          if (body.length > 0) {
            console.log("*** Cuenta encontrada ***");
            console.log("IBAN :" + body[0].IBAN + " / Saldo :" + body[0].balance);
            var response = body[0];
          } else {
            var response = {
              "msg" : "IBAN no encontrado"
            }
            res.status(404);
          }
        }
        res.send(response);
    }
  )
}

function updateAccount(IBAN, imPoper){
  console.log("6.1 Estoy en update account");
  console.log("6.2      IBAN     :" + IBAN);
  console.log("6.3      Importe  :" + imPoper);

  var query = 'q={"IBAN":' + '"' + IBAN + '"' + '}';

  console.log("6.4 La query es :" + query);
  //
  //Crea cliente http
  //
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("6.5 Cliente creado para actualiza balance");
  //
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, bodyGet) {

        if (bodyGet.length > 0) {
          var response = bodyGet[0];
          console.log("6.6 req.body.IBAN     : " + bodyGet[0].IBAN);
          console.log("6.7 req.body.balance  : " + bodyGet[0].balance);
          console.log("6.8 importe mvto.     : " + imPoper);

          var saldoFinal = parseFloat(bodyGet[0].balance) + parseFloat(imPoper);

          console.log("6.9.total saldo       : " + saldoFinal);

          var putBody = '{"$set":{"balance":' + saldoFinal + '}}';

          console.log("6.10 putBody :" + putBody);
          httpClient.put("account?" + query + "&" + mLabAPIKey,JSON.parse(putBody))
        } else {
          var response = {
            "msg" : "IBAN no encontrado"
          }
          console.log("Error 500");
        }
      }
    )
}

module.exports.getAccountById=getAccountById;
module.exports.updateAccount=updateAccount;
module.exports.getAccountByIBAN=getAccountByIBAN;
module.exports.altaContrato=altaContrato;
