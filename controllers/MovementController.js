const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyecto2019/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const fecha = require('../fecha');

const accountController = require('./AccountController');
const authController = require('./AuthController');
const userController = require('./UserController');
const movementController = require('./MovementController');

//
// GET USER BY ID
//
function getMovementsByIBAN(req, res) {
  console.log("GET /proyecto2019/movements");
  console.log("el body completo :" + req.body[0]);
  var IBAN = req.body.IBAN;
  var query = 'q={"IBAN":' + '"' + IBAN + '"' + '}';
  console.log("La query del movimiento :" + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para GET MOVEMENT BY IBAN");

  httpClient.get("movement?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
        if (err) {
            var response = {
              "msg" : "error obteniendo movimientos by IBAN"
            }
            res.status(500);
        } else {
          if (body.length > 0) {
            var response = body;
          } else {
            var response = {
              "msg" : "Movimiento no encontrado"
            }
            res.status(404);
          }
        }
        res.send(response);
    }
  )
}
/*
//
// POST create movement
//
function createMovement(req, res) {
  console.log("POST /create/movement");
  console.log(req.body.IBAN);
  console.log(req.body.imPoper);
  console.log(req.body.fecPoper);
  console.log(req.body.concepto);

  var newMovement = {
    "IBAN": req.body.IBAN,
    "imPoper": req.body.imPoper,
    "fecPoper": req.body.fecPoper,
    "concepto": req.body.concepto
  }

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para POST Movement");

  httpClient.post("movement?" + mLabAPIKey, newMovement,
    function(err, resMLab, body) {
          console.log("Movimiento creado con exito en MLab");
          res.status(201).send({"msg":"Movimiento guardado"});
    }
  )
}
*/

//
// POST create movement ingreso y actualiza saldo
//
function ingresoMovement(req, res) {
  console.log("1. POST /create/movement (ingreso)");
  console.log(req.body.IBAN);
  console.log(req.body.imPoper);

  var today = fecha.obtenerFecha();
  console.log("---> Fecha en codigo : " + today);

  var newMovement = {
    "IBAN": req.body.IBAN,
    "imPoper": req.body.imPoper,
    "fecPoper": today,
    "concepto": "Ingreso"
  }

  console.log("2. newMovement :" + newMovement);

  insertMovement(newMovement, res);

  console.log("3. Vuelvo de insertMovement");

  console.log("4. Voy a apdateAccount");
  console.log("5. IBAN        :" + req.body.IBAN);
  console.log("5.2 Importe    :" + req.body.imPoper);

  accountController.updateAccount(req.body.IBAN, req.body.imPoper);

  console.log("7. Vuelvo de updateAccount / Modificar saldo");

}

//
// POST create movement reintegro y actualiza saldo
//
function reintegroMovement(req, res) {
  console.log("1. POST /create/movement (reintegro)");
  console.log(req.body.IBAN);
  console.log(req.body.imPoper);

  var today = fecha.obtenerFecha();
  console.log("---> Fecha en codigo : " + today);

  var importe = req.body.imPoper * (-1);

  var newMovement = {
    "IBAN": req.body.IBAN,
    "imPoper": importe,
    "fecPoper": today,
    "concepto": "Reintegro"
  }


  console.log("2. newMovement :" + newMovement);

  insertMovement(newMovement, res);

  console.log("3. Vuelvo de insertMovement");

  console.log("4. Voy a apdateAccount");
  console.log("5. IBAN        :" + req.body.IBAN);
  console.log("5.2 Importe    :" + req.body.imPoper);

  accountController.updateAccount(req.body.IBAN, importe);

  console.log("7. Vuelvo de updateAccount / Modificar saldo");

}

function insertMovement(newMovement, res) {
    console.log("2.1 *** En la función insertMovement ***");
    console.log(newMovement.IBAN);
    console.log(newMovement.imPoper);
    console.log(newMovement.fecPoper);
    console.log(newMovement.concepto);

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("2.2    Cliente creado para POST insertMovement");

    httpClient.post("movement?" + mLabAPIKey, newMovement,
    function(err, resMLab, body) {
             console.log("2.3    Movimiento creado con exito en MLab");
             res.status(201).send({"msg":"Movimiento guardado"});
    }
  )
}
//
//module.exports.createMovement=createMovement;
module.exports.getMovementsByIBAN=getMovementsByIBAN;
module.exports.ingresoMovement=ingresoMovement;
module.exports.reintegroMovement=reintegroMovement;
module.exports.insertMovement=insertMovement;
