require('dotenv').config();

const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

app.use(express.json()); // Pasa a JSON todo lo que se deja en el body
app.use(enableCORS);

const userController = require('./controllers/UserController'); // ruta del controlador
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
const movementController = require('./controllers/MovementController2');

app.listen(port);
console.log("API escuchando en el puerto de AFE-SGA beep beep bepp " + port);

//
app.get('/proyecto2019/users', userController.getUsers)
app.get('/proyecto2019/users/:id', userController.getUsersById)
app.post('/proyecto2019/users', userController.createUser)
app.post('/proyecto2019/login', authController.loginUser)
app.post('/proyecto2019/logout', authController.logoutUser)
app.post('/proyecto2019/movement', movementController.getMovementsByIBAN)

app.get('/proyecto2019/account/:idUsuario', accountController.getAccountById)
//app.get('/proyecto2019/account', accountController.getAccountByIBAN)


app.post('/proyecto2019/altaContrato', accountController.altaContrato)


//app.get('/proyecto2019/movement', movementController.getMovementsByIBAN)
//app.post('/proyecto2019/movement', movementController.createMovement)
app.post('/proyecto2019/altaMovement', movementController.altaMovement)
//app.post('/proyecto2019/movement', movementController.reintegroMovement)
//
